FROM python:3-alpine

WORKDIR /app

ADD . /app

RUN apk update && apk upgrade

RUN apk add --no-cache linux-headers musl-dev libffi-dev build-base \
    openssl-dev nginx git gcc python3-dev python-dev

RUN python3 -m venv env && /app/env/bin/pip install --no-cache-dir -r \
    requirements.txt && /app/env/bin/pip install --no-cache-dir \
    git+https://github.com/Supervisor/supervisor

RUN cp classsurvey.conf /etc/nginx/conf.d

RUN env/bin/supervisord -c supervisord.conf

EXPOSE 80

CMD ["nginx"]

