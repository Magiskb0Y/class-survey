"""Implement forms for application
"""

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired, Length, ValidationError
from flask_wtf.file import FileField, FileAllowed



class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class UploadUser(FlaskForm):
    user_type = SelectField(
        'User type',
        choices=[('teacher', 'Teacher'), ('student', 'Student')]
    )
    excel = FileField('', validators=[FileAllowed(['xls', 'xlsx'])])
    submit = SubmitField('Upload')


class UploadSubjectClass(FlaskForm):
    survey_class = StringField('Survey class',validators=[DataRequired()])
    excel = FileField('', validators=[FileAllowed(['xls', 'xlsx'])])
    submit = SubmitField('Create')

