var admin = new Admin();
var authentication = new Authentication();
var site = new Site();


function Admin() {
    this.userManagement = new UserManagement();
    this.surveyClassManagement = new SurveyClassManagement();
    this.surveyCardManagement = new SurveyCardManagement();
}

$(function() {
    var $tabButtonItem = $('#tab-button li'),
        $tabSelect = $('#tab-select'),
        $tabContents = $('.tab-contents'),
        activeClass = 'is-active';

    $tabButtonItem.first().addClass(activeClass);
    $tabContents.not(':first').hide();

    $tabButtonItem.find('a').on('click', function(e) {
        var target = $(this).attr('href');

        $tabButtonItem.removeClass(activeClass);
        $(this).parent().addClass(activeClass);
        $tabSelect.val(target);
        $tabContents.hide();
        $(target).show();
        e.preventDefault();
    });

    $tabSelect.on('change', function() {
        var target = $(this).val(),
            targetSelectNum = $(this).prop('selectedIndex');

        $tabButtonItem.removeClass(activeClass);
        $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
        $tabContents.hide();
        $(target).show();
    });
});


function User() {
    let self = this;
    let authentication = new Authentication()
    this.id = "";
    this.username = "";
    this.fullname = "";
    this.email = "";
    this.user_type = "";
    this.course_id = "";
    this.private_key = authentication.getPrivateKey();

    this.fromServer = function() {
        let url = location.origin + '/api/user';
        $.ajax({
            url: url,
            type: 'GET',
            async: false,
            beforeSend:  function (request) {
                request.setRequestHeader('Private-Key', authentication.getPrivateKey());
            },
            success: function(response, statusCode, xhr) {
                if (response.code == 0) {
                    self.id        = response.user.uid;
                    self.username  = response.user.username;
                    self.fullname  = response.user.fullname;
                    self.email     = response.user.email;
                    self.user_type = response.user.user_type;
                    self.course_id = response.user.course_id;
                }
                else {
                }
            }
        });
    }

    this.fromRow = function(e){
        let row = e.parentNode.parentNode;
        this.id = parseInt(row.cells[0].textContent);
        this.username = row.cells[1].textContent;
        this.fullname = row.cells[2].textContent;
        this.email = row.cells[3].textContent;
        this.user_type = row.cells[4].textContent;
        this.course_id = row.cells[5].textContent;
        return this;
    }

    this.fromModal = function(modal) {
        this.id        = modal.find("#updateFormId").val();
        this.username  = modal.find("#updateFormUsername").val();
        this.fullname  = modal.find("#updateFormFullname").val();
        this.email     = modal.find("#updateFormEmail").val();
        this.user_type = modal.find("#updateFormUserType").val();
        this.course_id = modal.find("#updateFormCourseId").val();
        return this;
    }

    this.update = function() {
        let url = window.location.origin + '/api/user/' + this.id;
        let payload = {
            "uid"      : this.id,
            "username" : this.username,
            "fullname" : this.fullname,
            "email"    : this.email,
            "user_type": this.user_type,
            "course_id": this.course_id
        }
        $.ajax({
            url: url,
            type: "PUT",
            beforeSend: function(request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(payload),
            success: function (response, statusCode, xhr) {
                if (response.code == 0) {
                    let admin = new Admin();
                    let row = admin.userManagement.findRowById(self.id);
                    admin.userManagement.bindRowFromUser(row, self);
                    toastr.success(response.message);
                }
                else {
                    toastr.error(response.message);
                }
                $("#userModal").modal("hide");
            },
            error: function (xhr, textStatus, error) {
                console.log(error);
            }
        });
    }

    this.delete = function() {
        let url = location.origin + "/api/user/" + this.id;
        $.ajax({
            url: url,
            type: "DELETE",
            beforeSend: function(request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function(response, statusCode, xhr) {
                if (response.code == 0) {
                    let admin = new Admin();
                    toastr.success(response.message);
                    admin.userManagement.removeRowById(self.id);
                }
                else {
                    toastr.error(response.message);
                }
                $("#userModal").modal("hide");
            },
            error: function () {
                console.log("Request error");
            }
        })
    }

    this.setPassword = function (newPassword) {
        let url = location.origin + '/api/user/' + self.id + '/password';
        let payload = {
            new_password: newPassword
        }
        $.ajax({
            url: url,
            type: "PUT",
            dataType: 'json',
            contentType: "application/json",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.privateKey);
            },
            data: JSON.stringify(payload),
            success: function (response, statusCode, xhr) {
                if (response.code == 1) {
                    console.log(response);
                    toastr.error(response.message);
                }
                else {
                    toastr.success(response.message);
                }
            }
        });
    }

    this.resetPassword = function () {
        let url = location.origin + '/api/user/' + self.id + '/password';
        $.ajax({
            url: url,
            type: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.privateKey);
            },
            success: function (response, statusCode, xhr) {
                if (response.code == 1) {
                    console.log(response);
                    toastr.error(response.message);
                }
                else {
                    toastr.success(response.message);
                }
            }
        });
    }

}


function Authentication() {
    let self = this;
    this.privateKey = "";

    this.getPrivateKey = function () {
        let cookie = document.cookie;
        let pairs = cookie.split(';');
        for (let i = 0; i < pairs.length; ++i) {
            if (pairs[i].includes('private_key')) {
                self.privateKey = pairs[i].split('=')[1];
                break;
            }
        }
        return self.privateKey;
    }
}


function Site() {
    let self = this;
    let authentication = new Authentication();
    authentication.getPrivateKey();
    console.log(authentication);

    this.user = new User();
    this.user.fromServer();

    this.onClickSetPassword = function (e) {
        let newPassword = $("#formSetPassword").val();
        self.user.setPassword(newPassword);
    }

    this.submitReview = function () {
        let result = [];
        let p = $('input[type="radio"]');
        for (let i = 0; i < p.length; ++i) {
            if (p[i].checked) result.push(parseInt(p[i].value));
        }
        let payload = {
            id_class: document.getElementById("id_class").innerHTML,
            data: result
        };
        console.log("Payload", payload);
        let url = location.origin + '/api/user';
        $.ajax({
            url: url,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(payload),
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.privateKey);
            },
            success: function (response, statusCode, xhr) {
                if (response.code == 1) {
                    console.log(response);
                    toastr.error(response.message);
                }
                else {
                    toastr.success(response.message);
                }
            }
        });
        return result;
    }

}


function UserManagement() {
    let self = this;

    this.onClickView = function (e){
        let modal = $('#userModal');
        let user = new User();
        user.fromRow(e);
        self.bindModalFromUser(modal, user);
        modal.modal();
    }

    this.onClickRemove = function(e) {
        let user = new User();
        user.fromRow(e);
        user.delete();
    }

    this.onClickSubmit = function (e) {
        let modal = $('#userModal');
        let user = new User();
        user.fromModal(modal);
        user.update();
        modal.modal('hide');
    }

    this.onClickDelete = function (e) {
        let modal = $('#userModal');
        let user = new User();
        user.fromModal(modal);
        user.delete();
        modal.modal('hide');
    }

    this.onClickResetPassword = function (e) {
        let user = new User();
        user.fromRow(e);
        let newPassword = user.resetPassword();
    }

    this.bindRowFromUser = function (row, user){
        row.cells[1].innerHTML = user.username;
        row.cells[2].innerHTML = user.fullname;
        row.cells[3].innerHTML = user.email;
        row.cells[4].innerHTML = user.user_type;
        row.cells[5].innerHTML = user.course_id;
    }

    this.bindModalFromUser = function (modal, user) {
        modal.find("#updateFormId").val(user.id);
        modal.find("#updateFormUsername").val(user.username);
        modal.find("#updateFormFullname").val(user.fullname);
        modal.find("#updateFormEmail").val(user.email);
        modal.find("#updateFormUserType").val(user.user_type);
        modal.find("#updateFormCourseId").val(user.course_id);
    }

    this.findRowById = function (id) {
        let rows = $("#table-user table tbody tr td:first-child").parent();
        for (let i = 0; i < rows.length; ++i) {
            if (rows[i].cells[0].innerHTML == id) return rows[i];
        }
        return null;
    }

    this.removeRowById = function (id) {
        let row = self.findRowById(id);
        row.remove();
    }

    this.searchEvent = function (input) {
        url = window.location.origin + '/api/user/search?keyword='+input.value;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function (response, statusCode, xhr) {
                let tbody = $("#table-user table tbody");
                tbody.find("tr").remove();
                for (let i = 0; i < response.users.length; ++i) {
                    let row = '<tr>';
                    row += '<td>' + response.users[i].uid + '</td>';
                    row += '<td>' + response.users[i].username + '</td>';
                    row += '<td>' + response.users[i].fullname + '</td>';
                    row += '<td>' + response.users[i].email + '</td>';
                    row += '<td>' + response.users[i].user_type + '</td>';
                    row += '<td>' + response.users[i].course_id + '</td>';
                    row += '<td class="behavior"><span onclick="admin.userManagement.onClickView(this)" class="glyphicon glyphicon-eye-open"></span></td>';
                    row += '<td class="behavior"><span onclick="admin.userManagement.onClickRemove(this)" class="glyphicon glyphicon-remove"></span></td>';
                    row += '<td class="behavior"><span onclick="admin.userManagement.onClickResetPassword(this)" class="glyphicon glyphicon-refresh"></span></td>';
                    row += '</tr>';
                    tbody.append(row);
                }
            }
        })
    }
}


function SurveyClassManagement() {
    let self = this;
    let survey = {
        id: null,
        title: "",
        contents: []
    };

    this.onBlurSearch = function (e) {
        console.log(e);
        $("#survey-class").find("li.resultItem").remove();
    }

    this.updateReview = function () {
        // Update review panel
        let title = $('#survey-class div.left fieldset legend');
        title.text('Review: ' + survey.title);
        $("#nameSurveyClass").val(survey.title);
        let root = $('#reviewClassSurvey');
        root.empty();
        for (let iGr = 0; iGr < survey.contents.length; ++iGr) {
            let idGr = survey.contents[iGr].id;
            let titleGr = survey.contents[iGr].title;
            let criterials = survey.contents[iGr].contents;
            let lis = '';
            for (let iCr = 0; iCr < criterials.length; ++iCr) {
                lis += '<li class="list-group-item" id="' + criterials[iCr].id + '">'+ criterials[iCr].title + '<span class="deleteCriterial glyphicon glyphicon-remove" onclick="admin.surveyClassManagement.onClickDeleteCriterial(this)"></span>' + '</li>';
            }
            let gr = '<li class="list-group-item" id="' + idGr + '">' + titleGr + '<span class="deleteGroup glyphicon glyphicon-remove" onclick="admin.surveyClassManagement.onClickDeleteGroup(this)"></span>' + '<ul class="list-group">' + lis + '</ul></li>';
            root.append(gr);
        }

        // Update other control
        let selectGr = $("#group");
        selectGr.empty();
        for (let i = 0; i < survey.contents.length; ++i) {
            let idGr = survey.contents[i].id;
            let titleGr = survey.contents[i].title;
            let opt = '<option value="' + idGr + '">' + titleGr + "</option>";
            selectGr.append(opt);
        }
    }

    this.updateNameSurveyClass = function (e) {
        let surveyName = $('#nameSurveyClass').val();
        survey.title = surveyName;
        self.updateReview();
    }

    this.onClickAddGroup = function(e){
        let titleGr = $("#newGroupName").val();
        let idGr = survey.contents.length;
        if (titleGr.length == 0) {
            toastr.error("Title of survey can not empty");
        }
        else {
            let newGroup = {
                id: idGr,
                title: titleGr,
                contents: []
            };
            survey.contents.push(newGroup);
            self.updateReview();
        }

    }

    this.onClickAddCriterial = function(e) {
        let titleCriterial = $("#newCriterial").val();
        let idGr = $("#group").val();
        let group = null;
        for (let i = 0; i < survey.contents.length; ++i) {
            if (survey.contents[i].id == idGr) {
                group = survey.contents[i];
                break;
            }
        }
        let idCr = group.contents.length;
        let criterial = {
            id: idCr,
            title: titleCriterial
        };
        group.contents.push(criterial);
        self.updateReview();
    }

    this.onClickReset = function (e) {
        if (survey.id != null){
            survey.name = "";
            survey.contents = [];
        }
        else {
            survey = {
                id: null,
                name: "",
                contents: []
            };
        }

        self.updateReview();
    }

    this.onClickDeleteGroup = function (e) {
        let li = e.parentNode;
        let idGr = li.id;
        let idx = null;
        for (i = 0; i < survey.contents.length; ++i) {
            if (survey.contents[i].id == idGr) {
                idx = i;
                break;
            }
        }
        if (idx != null) {
            survey.contents.splice(idx, 1);
            self.updateReview();
        }
    }

    this.onClickDeleteCriterial = function (e) {
        let li = e.parentNode;
        let idCr = li.id;
        let idGr = li.parentNode.parentNode.id;
        let idx = null;
        for (i = 0; i < survey.contents.length; ++i) {
            if (survey.contents[i].id == idGr) {
                let gr = survey.contents[i];
                for (let j = 0; j < gr.contents.length; ++j) {
                    if (gr.contents[j].id == idCr) {
                        idx = j;
                        break;
                    }
                }
                if (idx != null) {
                    gr.contents.splice(idx, 1)
                    self.updateReview();
                }
                break;
            }
        }
    }

    this.onClickDelete = function (e) {
        if (survey.id == null){
            toastr.error("Survey not exist");
            return;
        }
        let url = location.origin + '/api/surveyclass/' + survey.id;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: "DELETE",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function (response, statusCode, xhr) {
                if (response.code == 0) {
                    toastr.success(response.message);
                    survey = {
                        id: null,
                        title: "",
                        schema: ""
                    };
                    self.updateReview();
                }
                else {
                    toastr.error(response.message);
                }
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }

    this.onClickSubmit = function (e) {
        let url = location.origin + '/api/surveyclass';
        let authentication = new Authentication();
        console.log(survey);
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            data: JSON.stringify(survey),
            success: function (response, statusCode, xhr) {
                if (response.code == 0) {
                    toastr.success(response.message);
                }
                else {
                    toastr.error(response.message)
                }
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }

    this.onClickUpdate = function (e) {
        console.log(survey);
        if (survey.id == null) {
            toastr.error("Survey not exist");
            return;
        }
        let url = location.origin + '/api/surveyclass';
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            data: JSON.stringify(survey),
            success: function (response, statusCode, xhr) {
                console.log(response);
                if (response.code == 0) {
                    toastr.success(response.message);
                }
                else {
                    toastr.error(response.message);
                }
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }

    this.onClickDeleteSurvey = function (e) {
        let id = e.getAttribute("sid");
        let url = location.origin + '/api/surveyclass/' + id;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: "DELETE",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function (response, statusCode, xhr) {
                console.log(response);
                toastr.success(response.message);
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }

    this.onClickSurveyItem = function (e) {
        let id = e.getAttribute("sid");
        let url = location.origin + '/api/surveyclass/' + id;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function(response, statusCode, xhr) {
                survey = response.survey;
                $("#resultSurvey").find("li.resultItem").remove();
                self.updateReview();
            },
            error: function (xhr, statusCode, error){
                console.log(error);
            }
        });

    }

    this.onSearchEvent = function (e) {
        let url = location.origin + '/api/surveyclass/search?keyword=' + e.value;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function (response, statusCode, xhr) {
                if (response.code == 0) {
                    let root = $("#survey-class .live-search .resultSurvey");
                    root.find("li.resultItem").remove();
                    for (let i = 0; i < response.survey.length; ++i) {
                        let li = `<li sid="${response.survey[i].id}" class="resultItem" onclick="admin.surveyClassManagement.onClickSurveyItem(this);">${response.survey[i].title}<span sid="${response.survey[i].id}" class="deleteSurvey" onclick="admin.surveyClassManagement.onClickDeleteSurvey(this)">x</span></li>`;
                        root.append(li);
                    }
                }
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }
}

function SurveyCardManagement () {
    let self = this;

    this.onSearchEvent = function (e) {
        let url = location.origin + '/api/surveyclass/search?keyword=' + e.value;
        let authentication = new Authentication();
        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function (request) {
                request.setRequestHeader("Private-Key", authentication.getPrivateKey());
            },
            success: function (response, statusCode, xhr) {
                if (response.code == 0) {
                    let root = $("#survey-card .live-search .resultSurvey");
                    root.find("li.resultItem").remove();
                    for (let i = 0; i < response.survey.length; ++i) {
                        let li = `<li surveytitle="${response.survey[i].title}" class="resultItem" onclick="admin.surveyCardManagement.onSelectSurvey(this);">${response.survey[i].title}</li>`;
                        root.append(li);
                    }
                }
                else {
                    console.log(response.message)
                }
            },
            error: function (xhr, statusCode, error) {
                console.log(error);
            }
        });
    }

    this.onSelectSurvey = function (e) {
        $("#survey-card .live-search input").val(e.getAttribute('surveytitle'));
        $("ul.resultSurvey").find("li").remove();
    }

    this.onBlurSearch = function (e) {
        $("#survey-card .live-search .resultSurvey").find("li.resultItem").remove();
    }
}
