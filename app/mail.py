"""Define mail module
"""

from flask_mail import Mail, Message

mail = Mail()


def send(subject='', recipients=[], body=''):
    global mail
    msg = Message(subject, recipients, body)
    mail.send(msg)
