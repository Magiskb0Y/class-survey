"""Define module site
"""

from flask import Blueprint


site_bp = Blueprint('site', __name__, template_folder='templates',
                     static_folder='static', static_url_path='/site/static')


from . import routes
