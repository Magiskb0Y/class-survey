"""Define routes for site module
"""

import json
from flask import render_template, redirect, url_for
from sqlalchemy import desc
from flask_login import current_user

from app.site import site_bp
from app import models


@site_bp.route('/')
def index():
    limit = 20
    if current_user.is_anonymous:
        return redirect(url_for('auth.login'))
    def set_view(x):
        x.status = True
        return x
    query = models.db.session.query(models.Notify).with_parent(current_user)
    notifies = query.order_by(desc(models.Notify.on_create)).all()

    notifies = list(map(set_view, notifies))
    models.db.session.add_all(notifies)
    return render_template('site/index.html', notifies=notifies)


@site_bp.route('/about')
def about():
    return render_template('about.html')


@site_bp.route('/dashboard')
def dashboard():
    return render_template('site/dashboard.html')


@site_bp.route('/profile')
def profile():
    return render_template('site/profile.html')

@site_bp.route('/review/<id_subject_class>')
def review(id_subject_class):
    ins = models.SubjectClass2ClassSurvey.query.filter_by(id_subject_class=id_subject_class).first()
    survey = models.SurveyClass.query.filter_by(sid=ins.id_class_survey).first()
    schema = json.loads(survey.schema)
    return render_template('site/review.html', schema=schema, id_class=id_subject_class)