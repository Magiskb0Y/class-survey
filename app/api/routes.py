"""Define resources
"""

import os
import json

from flask import request, jsonify, abort

from app.api import api_bp, auth_api
from app import models
from app import helper
from app import mail


@api_bp.route('/user/search', methods=['GET'])
@auth_api(admin_required=True)
def search():
    result = {}
    keyword = request.args.get('keyword')
    if keyword is None:
        try:
            users = models.User.get_all(user_type=['teacher', 'student'])
        except Exception as error:
            result.update(message='Search user failure', code=1)
        else:
            return users
    else:
        try:
            users = models.User.search(keyword, user_type=['teacher', 'student'])
        except Exception as error:
            result.update(message='Search user failure', code=1)
        else:
            result.update(users=users, code=0)
    return jsonify(**result)


@api_bp.route('/user', defaults={'uid': None}, methods=['GET'])
@api_bp.route('/user/<int:uid>', methods=['PUT', 'DELETE'])
@auth_api(admin_required=False)
def user(uid):
    result = {}
    if request.method == 'GET':
        private_key = request.headers.get('Private-Key')
        user = models.User.query.filter_by(private_key=private_key).first()
        result.update(user=user.to_json(), code=0)
    elif request.method == 'PUT':
        user_payload = request.get_json()
        try:
            user = models.User.get_user_by_id(uid, serialization='object')
            user.update(**user_payload)
        except Exception as error:
            result.update(message='Update failure', traceback=str(error), code=1)
        else:
            notify = models.Notify(uid, 'Update', 'Your account updated')
            notify.save_to_db()
            result.update(message='Update success', code=0)
    else:
        try:
            user = models.User.get_user_by_id(uid, serialization='object')
            models.db.session.delete(user)
        except Exception as error:
            result.update(message='Delete failure', code=1)
        else:
            result.update(message='Delete success', code=0)

    return jsonify(**result)


@api_bp.route('/user/<int:uid>/password', methods=['POST', 'PUT'])
@auth_api(admin_required=False)
def reset_password(uid):
    result = {}
    if request.method == 'POST':
        user = models.User.get_user_by_id(uid, serialization='object')
        if user:
            try:
                new_password = helper.generate_key(length=8)
                user.password = new_password
            except Exception as error:
                result.update(message='Reset password failure', code=1, traceback=str(error))
            else:
                result.update(message='Reset password success', newPassword=new_password, code=0)
                notify = models.Notify(uid, 'Password reset', 'Password change to {}'.format(new_password))
                notify.save_to_db()
                mail.send('Reset password', [user.email], 'Your password is {}'.format(new_password))
        else:
            result.update(message='ID invalid', code=1)
    elif request.method == 'PUT':
        private_key = request.headers.get('Private-Key')
        user = models.User.query.filter_by(private_key=private_key).first()
        payload = request.get_json()
        if 'new_password' not in payload:
            return jsonify(message='New password is missing', code=1)
        user.password = payload.get('new_password')
        user.save_to_db()
        notify = models.Notify(uid, 'Update', 'Your password is {}'.format(payload.get('new_password')))
        notify.save_to_db()
        result.update(message='Update password success', code=0)
    return jsonify(**result)


@api_bp.route('/surveyclass/search', methods=['GET'])
@auth_api(admin_required=True)
def search_survey():
    result = {}
    keyword = request.args.get('keyword')
    if keyword:
        try:
            conditional = models.SurveyClass.title.like('%{}%'.format(keyword))
            survey = models.SurveyClass.query.filter(conditional).limit(7)
            survey = list(map(lambda x: x.to_json(), survey))
        except Exception as error:
            result.update(message='Search survey failure', code=1)
        else:
            result.update(survey=survey, code=0)
    else:
        result.update(survey=[])
    return jsonify(**result)


@api_bp.route('/surveyclass/<int:sid>', methods=['GET', 'DELETE'])
@api_bp.route('/surveyclass', methods=['POST', 'PUT'], defaults={'sid': None})
@auth_api(admin_required=True)
def surveyclass(sid):
    if request.method == 'GET':
        result = {}
        survey = models.SurveyClass.query.filter_by(sid=sid).first()
        if survey is None:
            result.update(message='Survey not exist', code=1)
        else:
            result.update(survey=survey.to_json(), id=survey.sid, code=0)
        return jsonify(**result)

    elif request.method == 'POST':
        result = {}
        survey = request.get_json()
        try:
            survey_obj = models.SurveyClass.query.filter_by(title=survey.get('title')).first()
            if survey_obj:
                raise ValueError('Survey title existed')
            else:
                survey_obj = models.SurveyClass(survey.get('title'), json.dumps(survey['contents']))
                survey_obj.save_to_db()
        except Exception as error:
            result.update(message=str(error), code=1)
        else:
            result.update(message='Create survey success', id=survey_obj.sid, code=0)
        return jsonify(**result)

    elif request.method == 'PUT':
        result = {}
        payload = request.get_json()
        if 'id' not in payload:
            result.update(message='ID is missing', code=1)
        else:
            survey = models.SurveyClass.query.filter_by(sid=payload['id']).first()
            if survey is None:
                result.update(message='ID invalid', code=1)
            else:
                try:
                    survey.title = payload['title']
                    survey.schema = json.dumps(payload['contents'])
                    survey.save_to_db()
                except Exception as error:
                    result.update(message='Update survey error', traceback=str(error), code=1)
                else:
                    result.update(message='Update survey success', code=0)
        return jsonify(**result)
    else:
        result = {}
        survey = models.SurveyClass.query.filter_by(sid=sid).first()
        if survey is None:
            result.update(message='ID invalid', code=1)
        else:
            try:
                models.db.session.delete(survey)
                models.db.session.commit()
            except Exception as error:
                result.update(message='Delete survey error', code=1)
            else:
                result.update(message='Delete survey success', code=0)
        return jsonify(**result)

@api_bp.route('/user', methods=['POST'])
@auth_api(admin_required=False)
def submit_review():
    result = {}
    data = request.get_json()
    ins = models.SubjectClass2ClassSurvey.query.filter_by(id_subject_class=data['id_class']).first()
    r = []
    if len(ins.result) > 0:
        r = json.loads(ins.result)
    r.append(data['data'])
    ins.result = json.dumps(r)
    ins.save_to_db()
    result.update(message='Success', code=0)
    return jsonify(**result)