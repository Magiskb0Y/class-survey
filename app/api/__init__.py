"""Define REST API
"""


from functools import wraps
from flask import Blueprint, request, jsonify

from app import models


api_bp = Blueprint('api', __name__)


def auth_api(admin_required=False):
    def decorator(f):
        @wraps(f)
        def func(*args, **kwargs):
            private_key = request.headers.get('Private-Key')
            if private_key:
                user = models.User.query.filter_by(private_key=private_key).first()
                if user:
                    if admin_required:
                        if user.user_type == 'admin':
                            return f(*args, **kwargs)
                        else:
                            return jsonify(message='You are not admin', code=1)
                    else:
                        return f(*args, **kwargs)
                else:
                    return jsonify(message='Private key invalid', code=1)
            else:
                return jsonify(message='Private key is missing', code=1)
        return func
    return decorator


from . import routes

