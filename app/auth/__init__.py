"""Define module authentication
"""

from functools import wraps
from flask import Blueprint, redirect, url_for
from flask_login import current_user


auth_bp = Blueprint('auth', __name__, template_folder='templates',
                     static_folder='static')


from . import routes


def admin_required(f):
    @wraps(f)
    def func(*args, **kwargs):
        try:
            if current_user.user_type != 'admin':
                return redirect(url_for('site.index'))
            else:
                return f(*args, **kwargs)
        except Exception as error:
            return redirect(url_for('site.index'))
    return func
