"""Define routes for admin module
"""

from flask import render_template, current_app, url_for, redirect, abort, request, flash, make_response
from flask_login import login_user, logout_user, current_user

from app import models
from app.auth import auth_bp
from app.forms import LoginForm

@auth_bp.route('/login', methods=['POST', 'GET'])
def login():
    if not current_user.is_anonymous:
        return redirect(url_for('site.index'))
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        remember = form.remember.data
        user = models.User.query.filter_by(username=username).first()
        if user and user.verify_password(password):
            login_user(user, remember=remember)
            flash('Login success', 'info')
            if user.user_type == 'admin':
                response = make_response(redirect(url_for('admin.index')))
                response.set_cookie('private_key', user.private_key)
                return response
            else:
                response = make_response(redirect(url_for('admin.index')))
                response.set_cookie('private_key', user.private_key)
                return response
        else:
            error = 'Username or password invalid'
            return render_template('login.html', title='Login', form=form, error=error)

    return render_template('login.html', title='Login', form=form)


@auth_bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

