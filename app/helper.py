"""Define helper function
"""

import os
from random import SystemRandom
import openpyxl


__CHARS = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!0123456789'


def read_student_sheet(filename):
    """Read Excel file of List student
    Parameters:
    -----------
    filename: str, path to file Excel after uploaded

    Returns:
    --------
    student_record: list of dict
    """
    workbook = openpyxl.load_workbook(filename, read_only=True)
    sheet1 = workbook['Sheet1']
    student_record = []

    for i in range(2, sheet1.max_row+1):
        try:
            if not all([sheet1[i][1].value, sheet1[i][2].value, sheet1[i][3].value, sheet1[i][4].value, sheet1[i][5].value]):
                continue
            student = {
                'username' : str(sheet1[i][1].value),
                'password' : str(sheet1[i][2].value),
                'fullname' : str(sheet1[i][3].value),
                'email'    : '{}@vnu.edu.vn'.format(sheet1[i][1].value),
                'course_id': str(sheet1[i][5].value)
            }
            assert all(student.values()), 'One of field student is error'
            student_record.append(student)
        except Exception as error:
            break;
    return student_record


def read_teacher_sheet(filename):
    """Read Excel file of List teacher
    Parameters:
    -----------
    filename: str, path to file Excel after uploaded

    Returns:
    --------
    teacher_record: list of dict
    """
    workbook = openpyxl.load_workbook(filename, read_only=True)
    sheet1 = workbook['Sheet1']
    teacher_record = []

    for i in range(2, sheet1.max_row+1):
        try:
            if not all([sheet1[i][1].value, sheet1[i][2].value, sheet1[i][3].value, sheet1[i][4].value]):
                continue
            teacher = {
                'username' : str(sheet1[i][1].value),
                'password' : str(sheet1[i][2].value),
                'fullname' : str(sheet1[i][3].value),
                'email'    : '{}@vnu.edu.vn'.format(sheet1[i][1].value),
            }
            assert all(teacher.values()), 'One of field student is error'
            teacher_record.append(teacher)
        except Exception as error:
            break;
    return teacher_record


def read_subject_class(excel):
    """
    Perform read excel file of subject class
    Parameters:
    -----------
    excel: file object with extension excel file

    Returns:
    --------
    result: dict
    """
    try:
        workbook = openpyxl.load_workbook(excel, read_only=True)
        sheet1 = workbook['DSLMH']
        result = {}
        result['term']             = sheet1[5][0].value
        result['teacher_id']       = sheet1[7][4].value
        result['time']             = sheet1[8][2].value
        result['id_subject_class'] = sheet1[9][2].value
        result['amphitheater']     = sheet1[8][5].value
        result['credits']          = sheet1[9][5].value
        result['name_subject']     = sheet1[10][2].value
        result['students_id'] = []
        for i in range(12, sheet1.max_row):
            try:
                if sheet1[i][1].value is None:
                    break
                result['students_id'].append(sheet1[i][1].value)
            except:
                pass
        return result
    except Exception as err:
        print(err)
        return {}


def generate_key(length):
    """
    Perform generate random key

    Parameters:
    -----------
    length: int, length of random key

    Returns:
    --------
    key: str
    """
    key = ''.join([SystemRandom().choice(__CHARS) for _ in range(length)])
    return key

