"""Seting all config in here
"""

import os


__basedir__ = os.getcwd()


class BaseConfig(object):
    """Base for config Flask application
    """
    DEBUG = True
    TESTING = False
    HOST = 'localhost'
    PORT = 5000
    SECRET_KEY = 'nqy8lGfj3hbILSdtgRYioH7BUysAyLOi'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    TEMPLATES_AUTO_RELOAD = True
    EXPLAIN_TEMPLATE_LOADING = False
    MAIL_DEFAULT_SENDER = 'Class Survey'
    USERNAME_ROOT = 'root'
    PASSWORD_ROOT = 'password'


class DevelopmentConfig(BaseConfig):
    """Config for development stage
    """
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(__basedir__, 'site.db')
    TEMPLATES_AUTO_RELOAD = True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.environ.get('ADMIN_MAIL')
    MAIL_PASSWORD = os.environ.get('ADMIN_PWD')


class ProductConfig(BaseConfig):
    """Config for Production stage
    """
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://surveyclass:123456@localhost:3306/surveyclass'


def get(config_name):
    """Get config object by name
    Parameters:
    -----------
    config_name: str

    Returns:
    --------
    instance of BaseConfig
    """
    if config_name == 'development':
        return DevelopmentConfig
    elif config_name == 'production':
        return ProductConfig
    else:
        raise RuntimeError('No find config')
