"""Define database
"""

import json
from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_login import LoginManager, UserMixin

from app.helper import generate_key


db = SQLAlchemy()
login_manager = LoginManager()


@login_manager.user_loader
def load_user(uid):
    return User.query.get(int(uid))


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    uid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(10), unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    fullname = db.Column(db.String(50, convert_unicode=True), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    user_type = db.Column(db.String(10), nullable=False, default='student')
    course_id = db.Column(db.String(20), nullable=False)
    private_key = db.Column(db.String(64), nullable=False, default=generate_key(64))
    notifies = db.relationship('Notify', backref='users', cascade='delete,all', lazy=True)
    subject_class = db.relationship('SubjectClass', backref='users', cascade='delete,all', lazy=True)
    class_set = db.relationship('SubjectClass2Student', backref='users', cascade='delete,all', lazy=True)


    def __init__(self, username, password, fullname, email, user_type="student", course_id=""):
        self.username = username
        self.password = password
        self.fullname = fullname
        self.email = email
        self.user_type = user_type
        self.course_id = course_id

    def get_id(self):
        return self.uid

    def update(self, *args, **kwargs):
        if 'username' in kwargs:
            self.username = kwargs['username']
        if 'password' in kwargs:
            self.password = kwargs['password']
        if 'fullname' in kwargs:
            self.fullname = kwargs['fullname']
        if 'email' in kwargs:
            self.email = kwargs['email']
        if 'user_type' in kwargs:
            self.user_type = kwargs['user_type']
        if 'course_id' in kwargs:
            self.course_id = kwargs['course_id']
        self.save_to_db()

    @property
    def password(self):
        raise AttributeError('Attribute read only')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    @classmethod
    def search(cls, keyword, user_type=None, serialization='json'):
        limit_row = 20
        users = cls.query.filter(cls.username.like('%{}%'.format(keyword)))
        users = users.order_by(desc(cls.uid)).limit(limit_row)
        if user_type:
            users = filter(lambda x: x.user_type in user_type, users)
        if serialization == 'json':
            return list(map(lambda x: x.to_json(), users))
        elif serialization == 'object':
            return users
        else:
            return None

    def to_json(self):
        return {
                'uid': self.uid,
                'username': self.username,
                'fullname': self.fullname,
                'email': self.email,
                'course_id': self.course_id,
                'user_type': self.user_type,
                'private_key': self.private_key
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get_all(cls, user_type=[], serialization='json', limit=10):
        data = []
        for user_t in user_type:
            data.extend(cls.query.filter_by(user_type=user_t).order_by(cls.uid).limit(limit))
        if serialization == 'json':
            return list(map(lambda x: x.to_json(), data))
        elif serialization == 'object':
            return data

    @classmethod
    def get_user_by_id(cls, uid, serialization='json'):
        user = cls.query.filter_by(uid=uid).first()
        if user is not None:
            if serialization == 'json':
                return user.to_json()
            elif serialization == 'object':
                return user
        else:
            return None

    @classmethod
    def get_user_by_username(cls, username, serialization='json'):
        user = cls.query.filter_by(username=username).first()
        if user is not None:
            if serialization == 'json':
                return user.to_json()
            elif serialization == 'object':
                return user
        else:
            return None

    @classmethod
    def get_user_by_email(cls, email):
        user = cls.query.filter_by(email=email).first()
        if user is not None:
            return user.to_json()
        else:
            return None

    def from_dict(self, **kwargs):
        self.uid = kwargs.get('username')
        self.password = kwargs.get('password')
        self.email = kwargs.get('email')
        self.fullname = kwargs.get('fullname')
        self.course_id = kwargs.get('course_id', '')
        self.user_type = kwargs.get('user_type', 'student')


class Notify(db.Model):
    __tablename__ = 'notify'

    nid = db.Column(db.Integer(), primary_key=True)
    uid = db.Column(db.Integer, db.ForeignKey('users.uid'), nullable=False)
    title = db.Column(db.String(50), nullable=False)
    content = db.Column(db.String(200), nullable=True)
    status = db.Column(db.Boolean, default=False)
    on_create = db.Column(db.DateTime, default=datetime.now())


    def __init__(self, uid, title, content="", status=False):
        self.uid = uid
        self.title = title
        self.content = content
        self.status = status

    def to_json(self):
        return {
                'nid': self.nid,
                'uid': self.uid,
                'title': self.title,
                'content': self.content,
                'status': self.status,
                'on_create': self.on_create.strftime('%Y-%B-%D %H:%M:%S')
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

class SurveyClass(db.Model):
    __tablename__ = 'classsurvey'

    sid = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    schema = db.Column(db.String, nullable=False)


    def __init__(self, title, schema):
        self.title = title
        self.schema = schema

    def to_json(self):
        survey = {
            "id": self.sid,
            "title": self.title,
            "contents": json.loads(self.schema)
        }
        return survey

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class Subject(db.Model):
    __tablename__ = 'subjects'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

    def __init__(self, name):
        self.name = name

    def to_json(self):
        survey = {
            "id": self.id,
            "name": self.name
        }
        return survey

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class SubjectClass2ClassSurvey(db.Model):
    __tablename__ = 'subjectclass2classsurvey'

    id = db.Column(db.Integer, primary_key=True)
    id_class_survey = db.Column(db.Integer, db.ForeignKey('classsurvey.sid'), nullable=False)
    id_subject_class = db.Column(db.String(20), nullable=False)
    result = db.Column(db.String(100), nullable=False, default="")

    def __init__(self, id_class_survey, id_subject_class, result):
        self.id_class_survey = id_class_survey
        self.id_subject_class = id_subject_class
        self.result = result

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

class SubjectClass(db.Model):
    __tablename__ = 'subjectclass'

    id = db.Column(db.Integer, primary_key=True)
    term = db.Column(db.String(50), nullable=False)
    teacher_id = db.Column(db.Integer, db.ForeignKey('users.uid'), nullable=False)
    time = db.Column(db.String(50), nullable=False)
    id_subject = db.Column(db.Integer, db.ForeignKey('subjects.id'), nullable=False)
    amphitheater = db.Column(db.String(50), nullable=False)
    credits = db.Column(db.Integer, nullable=False)

    def __init__(self, term, teacher_id, time, id_subject, amphitheater, credits):
        self.term = term
        self.teacher_id = teacher_id
        self.time = time
        self.id_subject = id_subject
        self.amphitheater = amphitheater
        self.credits = credits

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class SubjectClass2Student(db.Model):
    __tablename__ = 'subjectclass2student'

    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.Integer, db.ForeignKey('users.uid'), nullable=False)
    subject_class_id = db.Column(db.Integer, db.ForeignKey('subjectclass.id'), nullable=False)

    def __init__(self, student_id, subject_class_id):
        self.student_id = student_id
        self.subject_class_id = subject_class_id

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
