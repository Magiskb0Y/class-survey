"""Define routes for admin module
"""

from flask import render_template, flash, redirect, url_for
from flask_login import logout_user, current_user, login_required
from sqlalchemy import desc

from app.admin import admin_bp
from app.auth import admin_required
from app import models
from app import forms
from app import helper


@admin_bp.route('/')
@admin_required
@login_required
def index():
    def set_view(x):
        x.status = True
        return x
    notifies = models.db.session.query(models.Notify).with_parent(current_user).filter_by(status=False).all()
    notifies = list(map(set_view, notifies))
    models.db.session.add_all(notifies)
    return render_template('admin/index.html', notifies=notifies)


@admin_bp.route('/management', methods=['POST', 'GET'])
@admin_required
@login_required
def management():
    limit_row = 20

    teachers = []
    students = []
    survey_class = []
    survey_card = []

    form = forms.UploadUser()
    create_survey_form = forms.UploadSubjectClass()
    users = models.User.query.order_by(desc(models.User.uid)).limit(limit_row)
    users = filter(lambda x: x.user_type != 'admin', users)

    if form.validate_on_submit():
        if form.excel.data:
            users = []
            if form.user_type.data == 'teacher':
                users = helper.read_teacher_sheet(form.excel.data)
            elif form.user_type.data == 'student':
                users = helper.read_student_sheet(form.excel.data)
            for user in users:
                user_object = models.User.query.filter_by(username=user['username']).first()
                if user_object is not None:
                    user_object.update(**user)
                else:
                    user.update(user_type=form.user_type.data)
                    user_object = models.User(**user)
                    user_object.save_to_db()
            flash('Upload list of {} success'.format(form.user_type.data), 'success')
            return redirect(url_for('admin.management'))
        else:
            error = 'Data required'
            return render_template('admin/management.html',
                    error=error,
                    form=form,
                    create_survey_form=create_survey_form,
                    users=users,
                    survey_class=survey_class,
                    survey_card=survey_card)

    elif create_survey_form.validate_on_submit():
        if create_survey_form.excel.data:
            try:
                class_survey = models.SurveyClass.query.filter_by(title=create_survey_form.survey_class.data).first()
                result = helper.read_subject_class(create_survey_form.excel.data)
                ins = models.SubjectClass2ClassSurvey(class_survey.sid, result['id_subject_class'], "")
                models.db.session.add(ins)
                models.db.session.commit()
            except Exception as err:
                print(err)
            # create notify for each student
            for uid in result['students_id']:
                try:
                    user = models.User.get_user_by_username(str(uid), serialization='object')
                    notify = models.Notify(user.uid, 'Thông báo', 'Đánh giá lớp môn học {} kèm link http://localhost:5000/review/{}'.format(result["id_subject_class"], result["id_subject_class"].replace(" ", "%20")))
                    notify.save_to_db()
                except Exception as err:
                    print(err)

            return redirect(url_for('admin.management'))
        else:
            return render_template('admin/management.html',
                    error=error,
                    form=form,
                    create_survey_form=create_survey_form,
                    users=users,
                    survey_class=survey_class,
                    survey_card=survey_card)

    return render_template('admin/management.html',
            form=form,
            create_survey_form=create_survey_form,
            users=users,
            survey_class=survey_class,
            survey_card=survey_card)


