"""Entry point for application
"""

import os
import logging

from flask import Flask
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell
from flask_moment import Moment
import babel

from . import helper
from . import config
from . import mail
from .mail import mail as mail_inst
from . import models
from . import forms
from . import admin
from . import site
from . import auth
from . import api

def create(config_name):
    """Create Flask app
    Parameters:
    -----------
    config_name: str

    Returns:
    --------
    instance of Flask
    """
    app = Flask(__name__)
    config_object = config.get(config_name)
    app.config.from_object(config_object)
    manager = Manager(app)
    moment = Moment(app)
    mail_inst.init_app(app)
    models.login_manager.init_app(app)
    models.db.init_app(app)
    migrate = Migrate(app, models.db)

    def format_datetime(value, format='full'):
        if format == 'full':
            format="EEEE, d. MMMM y 'at' HH:mm"
        elif format == 'medium':
            format="EE dd.MM.y HH:mm"
        return babel.dates.format_datetime(value, format)
    app.jinja_env.filters['datetime'] = format_datetime

    @app.after_request
    def apply_caching(response):
        """Solver issue CORS
        """
        response.headers["Access-Control-Allow-Origin"] = '*'
        response.headers["Access-Control-Allow-Methods"] = 'POST'
        response.headers["Access-Control-Allow-Headers"] = 'Content-Type'
        return response


    @app.before_request
    def before_request():
        app.jinja_env.cache.clear()

    def _make_context():
        return dict(app=app, db=models.db, models=models)

    manager.add_command('db', MigrateCommand)
    manager.add_command('shell', Shell(
        make_context=_make_context,
        use_ipython=True
    ))

    app.register_blueprint(auth.auth_bp, url_prefix='/auth')
    app.register_blueprint(admin.admin_bp, url_prefix='/admin')
    app.register_blueprint(site.site_bp, url_prefix='/')
    app.register_blueprint(api.api_bp, url_prefix='/api')

    if config_name == 'development':
        return manager
    elif config_name == 'production':
        return app

