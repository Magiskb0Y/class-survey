"""Generate wsgi instance
"""

import app

application = app.create('production')

